
# [fit] I've got a brand new

# [fit] Combine Publisher

## **(I'll give you the key)**

---

# [fit] What is this all for? 🤨

---

[.background-color: #666]
![inline](Workouts.png)

---

# Workouts sample query

A query to retrieve all workouts stored in health kit

```swift
extension HKSampleQuery {

    public static func workouts(
        completion: @escaping (HKSampleQuery, [HKSample]?, Error?) -> Void
    ) -> HKSampleQuery {

        let sampleType = HKWorkoutType.workoutType()
        let start = Date.distantPast
        let end = Date()
        let predicate = HKQuery.predicateForSamples(withStart: start, end: end, options: HKQueryOptions())
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let limit = HKObjectQueryNoLimit
        return HKSampleQuery(sampleType: sampleType,
                             predicate: predicate,
                             limit: limit,
                             sortDescriptors: [sortDescriptor],
                             resultsHandler: completion)
    }
}
```

---

# Average heart rate statistics query

A query to retrieve the average heart rate for a given workout

```swift
extension HKStatisticsQuery {

    public static func averageHeartRate(
        for workout: HKWorkout,
        completion: @escaping (HKStatisticsQuery, HKStatistics?, Error?) -> Void
    ) -> HKStatisticsQuery {

        let predicate = HKQuery.predicateForSamples(during: workout.activeIntervals)
        return HKStatisticsQuery(quantityType: .heartRate,
                                 quantitySamplePredicate: predicate,
                                 options: [.discreteAverage],
                                 completionHandler: completion)
    }
}
```

---

# Workout item

Provides both the workout and average heart rate information

```swift
struct WorkoutItem {
    let start: Date
    let end: Date
    let averageHeartRate: Double
}
```

---

# [fit] Traditional fetching 😎

---

# Fetch method with a completion block

```swift
extension HKHealthStore {

    func fetchWorkouts(completion: @escaping (Result<[WorkoutItem], Error>) -> Void) {

        requestAuthorization(toShare: nil, read: sampleTypes) { success, error in

            guard success else {
                completion(.failure(error!))
                return
            }

            let workoutsQuery = HKSampleQuery.workouts { _, result, error in

                guard let samples = result else {
                    completion(.failure(error!))
                    return
                }

                guard let workouts = samples as? [HKWorkout] else {
                    print("Samples are not workouts!")
                    return
                }

                let group = DispatchGroup()
                var result: [WorkoutItem] = []

                for workout in workouts {

                    group.enter()

                    let averageHeartRateQuery = HKStatisticsQuery.averageHeartRate(for: workout) {
                        query, averageHeartRateResult, error in

                        defer { group.leave() }

                        guard let averageHeartRate = averageHeartRateResult?.averageQuantity() else {
                            print("Can't get average heart rate! \(error!)")
                            return
                        }

                        result.append(WorkoutItem(workout: workout, averageHeartRate: averageHeartRate))
                    }

                    self.execute(averageHeartRateQuery)
                }

                group.notify(queue: .main) {
                    completion(.success(result))
                }
            }

            self.execute(workoutsQuery)
        }
    }
}
```

---

# Fetch method with a completion block

Request authorization from HealthKit to read the data

```swift
extension HKHealthStore {

    func fetchWorkouts(completion: @escaping (Result<[WorkoutItem], Error>) -> Void) {

        requestAuthorization(toShare: nil, read: sampleTypes) { success, error in

            guard success else {
                completion(.failure(error!))
                return
            }
```

---

# Fetch method with a completion block

Fetch all the workouts

```swift
let workoutsQuery = HKSampleQuery.workouts { _, result, error in

    guard let samples = result else {
        completion(.failure(error!))
        return
    }

    guard let workouts = samples as? [HKWorkout] else {
        print("Samples are not workouts!")
        return
    }
```

---

# Fetch method with a completion block

Fetch the associated average heart rate data for each workout

```swift
let group = DispatchGroup()
var result: [WorkoutItem] = []

for workout in workouts {

    group.enter()

    let averageHeartRateQuery = HKStatisticsQuery.averageHeartRate(for: workout) {
        query, averageHeartRateResult, error in

        defer { group.leave() }

        guard let averageHeartRate = averageHeartRateResult?.averageQuantity() else {
            print("Can't get average heart rate! \(error!)")
            return
        }

        result.append(WorkoutItem(workout: workout, averageHeartRate: averageHeartRate))
    }

    self.execute(averageHeartRateQuery)
}

group.notify(queue: .main) {
    completion(.success(result))
}
```

---

# Fetch method with a completion block

Oh and don't forget to execute the workouts query

😅

```swift
            self.execute(workoutsQuery)
        }
    }
}
```

---

# Fetch method with a completion block

```swift
extension HKHealthStore {

    func fetchWorkouts(completion: @escaping (Result<[WorkoutItem], Error>) -> Void) {

        requestAuthorization(toShare: nil, read: sampleTypes) { success, error in

            guard success else {
                completion(.failure(error!))
                return
            }

            let workoutsQuery = HKSampleQuery.workouts { _, result, error in

                guard let samples = result else {
                    completion(.failure(error!))
                    return
                }

                guard let workouts = samples as? [HKWorkout] else {
                    print("Samples are not workouts!")
                    return
                }

                let group = DispatchGroup()
                var result: [WorkoutItem] = []

                for workout in workouts {

                    group.enter()

                    let averageHeartRateQuery = HKStatisticsQuery.averageHeartRate(for: workout) {
                        query, averageHeartRateResult, error in

                        defer { group.leave() }

                        guard let averageHeartRate = averageHeartRateResult?.averageQuantity() else {
                            print("Can't get average heart rate! \(error!)")
                            return
                        }

                        result.append(WorkoutItem(workout: workout, averageHeartRate: averageHeartRate))
                    }

                    self.execute(averageHeartRateQuery)
                }

                group.notify(queue: .main) {
                    completion(.success(result))
                }
            }

            self.execute(workoutsQuery)
        }
    }
}
```

---

# [fit] Can Combine help? 🤔

---

# Create the publisher

```swift
public struct SamplePublisher {

    public typealias Output = [HKSample]
    public typealias Failure = Error

    fileprivate let store: HKHealthStore
    fileprivate let sampleType: HKSampleType
    fileprivate let predicate: NSPredicate?
    fileprivate let limit: Int
    fileprivate let sortDescriptors: [NSSortDescriptor]?
}
```

---

# Conform to the Publisher protocol

```swift
extension SamplePublisher: Publisher {

    public func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        store: store,
                                        sampleType: sampleType,
                                        predicate: predicate,
                                        limit: limit,
                                        sortDescriptors: sortDescriptors)
        subscriber.receive(subscription: subscription)
    }
}
```

---

```swift
extension SamplePublisher {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let store: HKHealthStore
        private let query: HKSampleQuery

        fileprivate init(subscriber: Subscriber,
                         store: HKHealthStore,
                         sampleType: HKSampleType,
                         predicate: NSPredicate?,
                         limit: Int,
                         sortDescriptors: [NSSortDescriptor]?) {

            self.store = store
            self.query = HKSampleQuery(sampleType: sampleType,
                                       predicate: predicate,
                                       limit: limit,
                                       sortDescriptors: sortDescriptors ) { _, samples, error in

                if let samples = samples {
                    _ = subscriber.receive(samples)
                    subscriber.receive(completion: .finished)
                } else {
                    subscriber.receive(completion: .failure(error!))
                }
            }
        }
    }
}
```

---

# Conform to the Subscription protocol

```swift
extension SamplePublisher.Subscription: Subscription {

    func request(_ demand: Subscribers.Demand) {
        store.execute(query)
    }

    func cancel() {
        store.stop(query)
    }
}
```

---

# Add an extension to provide the publisher

```swift
extension HKHealthStore {

    public func samplePublisher(
        sampleType: HKSampleType,
        predicate: NSPredicate?,
        limit: Int,
        sortDescriptors: [NSSortDescriptor]?
    ) -> SamplePublisher {

        SamplePublisher(store: self,
                             sampleType: sampleType,
                             predicate: predicate,
                             limit: limit,
                             sortDescriptors: sortDescriptors)
    }
}
```

---

# [fit] Wait! We already had a
# [fit] convenience initializer
# [fit] for a sample query! 😨

---

# Workouts sample query

A query to retrieve all workouts stored in health kit

```swift
extension HKSampleQuery {

    public static func workouts(
        completion: @escaping (HKSampleQuery, [HKSample]?, Error?) -> Void
    ) -> HKSampleQuery {

        let sampleType = HKWorkoutType.workoutType()
        let start = Date.distantPast
        let end = Date()
        let predicate = HKQuery.predicateForSamples(withStart: start, end: end, options: HKQueryOptions())
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let limit = HKObjectQueryNoLimit
        return HKSampleQuery(sampleType: sampleType,
                             predicate: predicate,
                             limit: limit,
                             sortDescriptors: [sortDescriptor],
                             resultsHandler: completion)
    }
}
```

---

# Create the publisher

```swift
public struct SamplePublisher2 {

    public typealias Output = [HKSample]
    public typealias Failure = Error

    typealias Completion = (HKSampleQuery, [HKSample]?, Error?) -> ()

    fileprivate let store: HKHealthStore
    fileprivate let query: (@escaping Completion) -> HKSampleQuery
}
```

---

# Conform to the Publisher protocol

```swift
extension SamplePublisher2: Publisher {

    public func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber, store: store, query: query)
        subscriber.receive(subscription: subscription)
    }
}
```

---

```swift
extension SamplePublisher2 {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let store: HKHealthStore
        private let query: HKSampleQuery
        fileprivate init(subscriber: Subscriber,
                         store: HKHealthStore,
                         query: (@escaping Completion) -> HKSampleQuery) {

            self.store = store
            self.query = query { _, output, failure in

                if let output = output {
                    _ = subscriber.receive(output)
                    subscriber.receive(completion: .finished)
                } else {
                    subscriber.receive(completion: .failure(failure!))
                }
            }
        }
    }
}
```

---

# Conform to the Subscription protocol

```swift
extension SamplePublisher2.Subscription: Subscription {

    func request(_ demand: Subscribers.Demand) {
        store.execute(query)
    }

    func cancel() {
        store.stop(query)
    }
}
```

---

# Add an extension to provide the publisher

```swift
extension HKHealthStore {

    public func publisher(
        for query: @escaping (@escaping (HKSampleQuery, [HKSample]?, Error?) -> ()) -> HKSampleQuery
    ) -> SamplePublisher2 {
        SamplePublisher2(store: self, query: query)
    }
}
```

---

# Using SamplePublisher2

```swift
let store = HKHealthStore()

let workoutsPublisher = store.publisher(for: {
    HKSampleQuery.workouts(completion: $0)
})
```
---

# Using SamplePublisher2

```swift
let store = HKHealthStore()

let workoutsPublisher = store.publisher(for: HKSampleQuery.workouts)
```
---

# [fit] Let's take a gander at the
# [fit] HealthKit queries 🧐

---

# [fit] A look at the HealthKit queries

```swift
init(sampleType: HKSampleType,
     predicate: NSPredicate?,
     limit: Int,
     sortDescriptors: [NSSortDescriptor]?,
     resultsHandler: @escaping (HKSampleQuery, [HKSample]?, Error?) -> Void)

init(quantityType: HKQuantityType,
     quantitySamplePredicate: NSPredicate?,
     options: HKStatisticsOptions,
     completionHandler: @escaping (HKStatisticsQuery, HKStatistics?, Error?) -> Void)

init(sampleType: HKSampleType,
     samplePredicate: NSPredicate?,
     completionHandler: @escaping (HKSourceQuery, Set<HKSource>?, Error?) -> Void)
```
---

# The completion closures

```swift
(HKStatisticsQuery, HKStatistics?,  Error?  ) -> Void

(HKSourceQuery,     Set<HKSource>?, Error?  ) -> Void

(HKSampleQuery,     [HKSample]?,    Error?  ) -> Void
```

---

# The completion closures

```swift
(HKStatisticsQuery, HKStatistics?,  Error?  ) -> Void

(HKSourceQuery,     Set<HKSource>?, Error?  ) -> Void

(HKSampleQuery,     [HKSample]?,    Error?  ) -> Void

   ⇓

(Query,
```

---

# The completion closures

```swift
(HKStatisticsQuery, HKStatistics?,  Error?  ) -> Void

(HKSourceQuery,     Set<HKSource>?, Error?  ) -> Void

(HKSampleQuery,     [HKSample]?,    Error?  ) -> Void

   ⇓                  ⇓

(Query,             Output?,
```

---

# These aren't three different closures!

```swift
(HKStatisticsQuery, HKStatistics?,  Error?  ) -> Void

(HKSourceQuery,     Set<HKSource>?, Error?  ) -> Void

(HKSampleQuery,     [HKSample]?,    Error?  ) -> Void

   ⇓                  ⇓              ⇓

(Query,             Output?,        Failure?) -> Void
```

---

# [fit] Let's make the publisher
# [fit] generic! 🤯

---

# Specific

```swift
public struct SamplePublisher2 {

    public typealias Output = [HKSample]
    public typealias Failure = Error

    typealias Completion = (HKSampleQuery, [HKSample]?, Error?) -> ()

    fileprivate let store: HKHealthStore
    fileprivate let query: (@escaping Completion) -> HKSampleQuery
}
```

---

# Generic

```swift
public struct QueryPublisher<Query: HKQuery, Output, Failure: Error> {

    typealias Completion = (Query, Output?, Failure?) -> ()

    fileprivate let store: HKHealthStore
    fileprivate let query: (@escaping Completion) -> Query
}
```

---

# Specific

```swift
extension SamplePublisher2: Publisher {

    public func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber, store: store, query: query)
        subscriber.receive(subscription: subscription)
    }
}
```

---

# Generic

```swift
extension QueryPublisher: Publisher {

    public func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber, store: store, query: query)
        subscriber.receive(subscription: subscription)
    }
}
```

---

# Specific

```swift
extension SamplePublisher2 {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let store: HKHealthStore
        private let query: HKSampleQuery
        fileprivate init(subscriber: Subscriber,
                         store: HKHealthStore,
                         query: (@escaping Completion) -> HKSampleQuery) {

            self.store = store
            self.query = query { _, output, failure in

                if let output = output {
                    _ = subscriber.receive(output)
                    subscriber.receive(completion: .finished)
                } else {
                    subscriber.receive(completion: .failure(failure!))
                }
            }
        }
    }
}
```

---

# Generic

```swift
extension QueryPublisher {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        private let store: HKHealthStore
        private let query: Query
        fileprivate init(subscriber: Subscriber,
                         store: HKHealthStore,
                         query: (@escaping Completion) -> Query) {

            self.store = store
            self.query = query { _, output, failure in

                if let output = output {
                    _ = subscriber.receive(output)
                    subscriber.receive(completion: .finished)
                } else {
                    subscriber.receive(completion: .failure(failure!))
                }
            }
        }
    }
}
```

---

# Specific

```swift
extension SamplePublisher2.Subscription: Subscription {

    func request(_ demand: Subscribers.Demand) {
        store.execute(query)
    }

    func cancel() {
        store.stop(query)
    }
}
```

---

# Generic

```swift
extension QueryPublisher.Subscription: Subscription {

    func request(_ demand: Subscribers.Demand) {
        store.execute(query)
    }

    func cancel() {
        store.stop(query)
    }
}
```

---

# Specific

```swift
extension HKHealthStore {

    public func publisher(
        for query: @escaping (@escaping (HKSampleQuery, [HKSample]?, Error?) -> ()) -> HKSampleQuery
    ) -> SamplePublisher2 {
        SamplePublisher2(store: self, query: query)
    }
}
```

---

# Generic

```swift
extension HKHealthStore {

    public func publisher<Query: HKQuery, Output, Failure: Error>(
        for query: @escaping (@escaping (Query, Output?, Failure?) -> ()) -> Query
    ) -> QueryPublisher<Query, Output, Failure> {
        QueryPublisher(store: self, query: query)
    }
}
```


---

# Using the generic QueryPublisher

```swift
let store = HKHealthStore()

let workouts = store.publisher(for: HKSampleQuery.workouts)
```

---

# Using the generic QueryPublisher

```swift
let store = HKHealthStore()

let workouts = store.publisher(for: HKSampleQuery.workouts)

let averageHeartRate = store.publisher(for: {
    HKStatisticsQuery.averageHeartRate(for: workout, completion: $0)
})
```

---

# [fit] The final reveal 😲

---

# Fetch method with a completion block

```swift
extension HKHealthStore {

    func fetchWorkouts(completion: @escaping (Result<[WorkoutItem], Error>) -> Void) {

        requestAuthorization(toShare: nil, read: sampleTypes) { success, error in

            guard success else {
                completion(.failure(error!))
                return
            }

            let workoutsQuery = HKSampleQuery.workouts { _, result, error in

                guard let samples = result else {
                    completion(.failure(error!))
                    return
                }

                guard let workouts = samples as? [HKWorkout] else {
                    print("Samples are not workouts!")
                    return
                }

                let group = DispatchGroup()
                var result: [WorkoutItem] = []

                for workout in workouts {

                    group.enter()

                    let averageHeartRateQuery = HKStatisticsQuery.averageHeartRate(for: workout) {
                        query, averageHeartRateResult, error in

                        defer { group.leave() }

                        guard let averageHeartRate = averageHeartRateResult?.averageQuantity() else {
                            print("Can't get average heart rate! \(error!)")
                            return
                        }

                        result.append(WorkoutItem(workout: workout, averageHeartRate: averageHeartRate))
                    }

                    self.execute(averageHeartRateQuery)
                }

                group.notify(queue: .main) {
                    completion(.success(result))
                }
            }

            self.execute(workoutsQuery)
        }
    }
}
```

---

# Using the publishers together

```swift
extension HKHealthStore {

    var workoutsPublisher: AnyPublisher<[WorkoutItem], Error> {

        self.requestAuthorization(read: sampleTypes)
            .flatMap { self.publisher(for: HKSampleQuery.workouts) }
            .compactMap { $0 as? [HKWorkout] }
            .flatMap { workouts in

                workouts.map { workout in

                    self.publisher(for: { HKStatisticsQuery.averageHeartRate(for: workout, completion: $0) })
                        .compactMap { $0.averageQuantity() }
                        .map { WorkoutItem(workout: workout, averageHeartRate: $0) }
                }
                .combineLatest
            }
            .eraseToAnyPublisher()
    }
}
```

---

# Using the publishers together

![inline](Publisher Chain.pdf)

---

# **Thank you**

### **@danielctull**