
# [fit] I've got a brand new
# [fit] Combine Publisher

## **(I'll give you the key)**

---

# [fit] What is Combine? 🤨

---

>>> The Combine framework provides a declarative Swift API for **processing values** over **time**.
-- Apple Documentation

---

>>> synchronous functions
>>> **in**
>>> asynchronous situations

---

# [fit] What is this all for? 🧐

---
# LinkPresentation

```swift
open class LPMetadataProvider : NSObject {

    func startFetchingMetadata(
        for URL: URL,
        completionHandler: @escaping (LPLinkMetadata?, Error?) -> Void
    )
}
```

---

# [fit] Traditional fetching 😒

---

# Traditional fetching

```swift
struct MetadataView: View {

    @State var title: String?
    var body: some View { Text(title ?? "") }

    func traditionalFetchMatadata(for url: URL) {

        LPMetadataProvider().startFetchingMetadata(for: url) {
            (metadata: LPLinkMetadata?, error: Error?) in

            guard let metadata = metadata else {
                print(error!.localizedDescription)
                return
            }

            DispatchQueue.main.async {
                self.title = metadata.title
            }
        }
    }
}
```

---


# [fit] Combine Fetching 😎

---

# Combine Fetching

```swift

struct CombineMetadataView: View {

    @State var title: String?
    var body: some View { Text(title ?? "") }

    @State var cancellable: AnyCancellable?

    func combineFetchMatadata(for url: URL) {

        cancellable = LPMetadataProvider()
            .metadata(for: url)
            .catch { _ in Empty<LPLinkMetadata, Never>() }
            .map(\.title)
            .receive(on: DispatchQueue.main)
            .assign(to: \.title, on: self)
    }
}
```

---

# [fit] How do we implement
# [fit] a publisher? 🤔

---

# Add an extension to provide the publisher

```swift
extension LPMetadataProvider {

    func metadata(for url: URL) -> LinkMetadataPublisher {
        LinkMetadataPublisher(provider: self, url: url)
    }
}
```

---

# Create the publisher

```swift
struct LinkMetadataPublisher {
    typealias Output = LPLinkMetadata
    typealias Failure = Error
    private let provider: LPMetadataProvider
    private let url: URL

    public init(provider: LPMetadataProvider, url: URL) {
        self.provider = provider
        self.url = url
    }
}
```

----

# Conform to the Publisher protocol

```swift
extension LinkMetadataPublisher: Publisher {

    func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        provider: provider,
                                        url: url)
        subscriber.receive(subscription: subscription)
    }
}
```

----

# Implement a subscription

```swift
extension LinkMetadataPublisher {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output {

        private let provider: LPMetadataProvider
        private let url: URL
        private let subscriber: Subscriber

        fileprivate init(subscriber: Subscriber,
                         provider: LPMetadataProvider,
                         url: URL) {
            self.subscriber = subscriber
            self.provider = provider
            self.url = url
        }
    }
}
```

---

# Conform to the Subscription protocol

```swift
extension LinkMetadataPublisher.Subscription: Combine.Subscription {

    func request(_ demand: Subscribers.Demand) {
        let subscriber = self.subscriber
        provider.startFetchingMetadata(for: url) { metadata, error in

            guard let metadata = metadata else {
                subscriber.receive(completion: .failure(error!))
                return
            }

            subscriber.receive(metadata)
            subscriber.receive(completion: .finished)
        }
    }

    func cancel() {}
}
```

---

# [fit] But wait! ⚠️
# [fit] What about all those other
# [fit] completion closure APIs??

---

# WebKit

```swift
open class WKWebView : NSView {

    func evaluateJavaScript(
        _ javaScriptString: String,
        completionHandler: ((Any?, Error?) -> Void)? = nil
    )
}
```

---

# CoreLocation

```swift
open class CLGeocoder : NSObject {

    func reverseGeocodeLocation(
        _ location: CLLocation,
        completionHandler: @escaping CLGeocodeCompletionHandler
    )
}

typealias CLGeocodeCompletionHandler = ([CLPlacemark]?, Error?) -> Void
```

---

# MapKit

```swift
open class MKLocalSearch : NSObject {

    func start(completionHandler: @escaping CompletionHandler)

    typealias CompletionHandler = (MKLocalSearch.Response?, Error?) -> Void
}
```

---

# The completion closures

```swift
(LPLinkMetadata?,         Error?  ) -> Void

((Any?,                   Error?  ) -> Void)? = nil

([CLPlacemark]?,          Error?  ) -> Void

(MKLocalSearch.Response?, Error?  ) -> Void
```

---

# The completion closures

```swift
(LPLinkMetadata?,         Error?  ) -> Void

((Any?,                   Error?  ) -> Void)? = nil

([CLPlacemark]?,          Error?  ) -> Void

(MKLocalSearch.Response?, Error?  ) -> Void

    ⇓                        ⇓           ⇓

(Output?,                 Failure?) -> Void
```

---

# [fit] Let's make the publisher
# [fit] generic! 🤯

---

# Specific

```swift
extension LinkMetadataPublisher.Subscription: Combine.Subscription {

    func request(_ demand: Subscribers.Demand) {
        let subscriber = self.subscriber
        provider.startFetchingMetadata(for: url) { metadata, error in

            guard let metadata = metadata else {
                subscriber.receive(completion: .failure(error!))
                return
            }

            subscriber.receive(metadata)
            subscriber.receive(completion: .finished)
        }
    }

    func cancel() {}
}
```

---

# Generic

```swift
extension CompletionPublisher.Subscription: Combine.Subscription {

    func request(_ demand: Subscribers.Demand) {
        let subscriber = self.subscriber
        perform { output, failure in

            guard let output = output else {
                subscriber.receive(completion: .failure(failure!))
                return
            }

            subscriber.receive(output)
            subscriber.receive(completion: .finished)
        }
    }

    func cancel() {}
}
```

---

# Specific

```swift
struct LinkMetadataPublisher {
    typealias Output = LPLinkMetadata
    typealias Failure = Error

    private let provider: LPMetadataProvider
    private let url: URL

    public init(provider: LPMetadataProvider, url: URL) {
        self.provider = provider
        self.url = url
    }
}
```

---

# Generic

```swift
struct CompletionPublisher<Output, Failure: Error> {


    typealias Completion = (Output?, Failure?) -> Void
    private let perform: (@escaping Completion) -> Void

    public init(perform: @escaping (@escaping Completion) -> Void) {
        self.perform = perform
    }
}
```

---

# Specific

```swift
extension LinkMetadataPublisher: Publisher {

    func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        provider: provider,
                                        url: url)
        subscriber.receive(subscription: subscription)
    }
}
```

---

# Generic

```swift
extension CompletionPublisher: Publisher {

    func receive<Subscriber>(subscriber: Subscriber)
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {
        let subscription = Subscription(subscriber: subscriber,
                                        perform: perform)

        subscriber.receive(subscription: subscription)
    }
}
```

---

# Specific

```swift
extension LinkMetadataPublisher {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output {

        private let provider: LPMetadataProvider
        private let url: URL
        private let subscriber: Subscriber

        fileprivate init(subscriber: Subscriber,
                         provider: LPMetadataProvider,
                         url: URL) {
            self.subscriber = subscriber
            self.provider = provider
            self.url = url
        }
    }
}
```

---

# Generic

```swift
extension CompletionPublisher {

    fileprivate final class Subscription<Subscriber>
        where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output {

        private let perform: (@escaping Completion) -> Void

        private let subscriber: Subscriber

        fileprivate init(subscriber: Subscriber,
                         perform: @escaping (@escaping Completion) -> Void) {

            self.subscriber = subscriber
            self.perform = perform

        }
    }
}
```

---

# Specific

```swift
extension LPMetadataProvider {

    func metadata(for url: URL) -> LinkMetadataPublisher {
        LinkMetadataPublisher(provider: self, url: url)
    }
}
```

---

# Generic

```swift
extension LPMetadataProvider {

    func metadata(for url: URL) -> CompletionPublisher<LPLinkMetadata, Error> {

        CompletionPublisher(perform: { completion in
            self.startFetchingMetadata(for: url, completionHandler: completion)
        })
    }
}
```

---

# WebKit

```swift
extension WKWebView {

    func evaluateJavascript(_ javaScript: String) -> CompletionPublisher<Any, Error> {

        CompletionPublisher(perform: { completion in
            self.evaluateJavaScript(javaScript, completionHandler: completion)
        })
    }
}
```

---

# CoreLocation

```swift
extension CLGeocoder {

    func reverseGeocodeLocation(_ location: CLLocation) -> CompletionPublisher<[CLPlacemark], Error> {

        CompletionPublisher(perform: { completion in
            self.reverseGeocodeLocation(location, completionHandler: completion)
        })
    }
}
```

---

# MapKit

```swift
extension MKLocalSearch {

    static func publisher(for request: MKLocalSearch.Request) -> CompletionPublisher<MKLocalSearch.Response, Error> {

        let request = MKLocalSearch(request: request)
        return CompletionPublisher(perform: request.start)
    }
}
```

---

# **Thank you**

### **@danielctull**

### **https://danieltull.co.uk**
