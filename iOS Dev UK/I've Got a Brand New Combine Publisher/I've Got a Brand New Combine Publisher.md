
# [fit] I've got a brand new

# [fit] Combine Publisher

## **(I'll give you the key)**

---

# The Goal

As a listener to the **radio**

I would like to see the **details** of the **currently playing song**

So that **I appear to know about music**

---

# Getting the timed metadata

![inline](AVPlayer.pdf)

---

# Fetching iTunes track data

* Observe the current item of the player
* Observe the timed metadata of the current item
* Search for the title in the iTunes API
* Decode the JSON
* Capture any errors and replace with nil
* Set to a property on the view (which causes it to reload)

---

# Publisher chaining

```swift
subscription = $player
    .flatMap { $0.publisher(for: \.currentItem) } // Observe currentItem
    .compactMap { $0 }
    .flatMap { $0?.publisher(for: \.timedMetadata) } // Observe timedMetadata
    .compactMap { $0?.title }
    .flatMap { term in

        urlSession
            .publisher(for: .search(term)) // Search for iTunes Item
            .map { $0.first } // Get first item
            .catch { _ in Just(nil) } // Silence error
    }
    .receive(on: DispatchQueue.main)
    .assign(to: \.iTunesItem, on: self)
```

^ This code observes the currentItem of an AVPlayer
and if there is one
then change it to observe the timed metadata
and if there is some
change it to a publisher of a network call to the iTunes search API to get an iTunes item for the text in the radio
where we get the first one
then we assign that iTunes item to the current item property

^ This will update the UI whenever there's a new currentItem or new timedMetadata once that network call finishes.

---

# Ummm, thanks Apple

![inline](Deprecated.png)

^ Left hand, meet right hand eh?

---

# Add an extension to provide the publisher

```swift
extension AVPlayerItem {

    public var metadataPublisher: MetadataPublisher {
        MetadataPublisher(item: self)
    }
}
```

---

# Create the publisher

```swift
public struct MetadataPublisher {
    fileprivate let item: AVPlayerItem
}
```

---

# Conform to the Publisher protocol

```swift
extension MetadataPublisher: Publisher {

    public typealias Output = AVTimedMetadataGroup
    public typealias Failure = Never

    public func receive<S>(subscriber: S)
        where
    S: Subscriber,
    S.Failure == Failure,
    S.Input == Output {

        let subscription = Subscription(item: item, subscriber: subscriber)
        subscriber.receive(subscription: subscription)
    }
}
```

---

```swift
extension MetadataPublisher {

    fileprivate final class Subscription<Subscriber>:
        NSObject,
        AVPlayerItemMetadataOutputPushDelegate
    where
        Subscriber: Combine.Subscriber,
        Subscriber.Failure == Failure,
        Subscriber.Input == Output
    {

        private let item: AVPlayerItem
        private let subscriber: Subscriber
        private let metadataOutput = AVPlayerItemMetadataOutput()

        fileprivate init(item: AVPlayerItem, subscriber: Subscriber) {
            self.item = item
            self.subscriber = subscriber
        }

        // AVPlayerItemMetadataOutputPushDelegate

        func metadataOutput(_: AVPlayerItemMetadataOutput,
                            didOutputTimedMetadataGroups groups: [AVTimedMetadataGroup],
                            from: AVPlayerItemTrack?) {

            groups.forEach { _ = subscriber.receive($0) }
        }
    }
}
```


---

# Conform to the Subscription protocol

```swift
extension MetadataPublisher.Subscription: Combine.Subscription {

    func request(_ demand: Subscribers.Demand) {
        let queue = DispatchQueue(label: "uk.co.danieltull.MetadataQueue")
        metadataOutput.setDelegate(self, queue: queue)
        item.add(metadataOutput)
    }

    func cancel() {
        item.remove(metadataOutput)
        metadataOutput.setDelegate(nil, queue: nil)
    }
}
```

---

# Using the new publisher

```swift
subscription = $player
    .flatMap { $0.publisher(for: \.currentItem) } // Observe currentItem
    .compactMap { $0 }
    .flatMap { $0.metadataPublisher } // Observe metadata
    .compactMap { $0.title }
    .flatMap { term in

        urlSession
            .publisher(for: .search(term)) // Search for iTunes Item
            .map { $0.first } // Get first item
            .catch { _ in Just(nil) } // Silence error
    }
    .receive(on: DispatchQueue.main)
    .assign(to: \.iTunesItem, on: self)
```

---

![inline autoplay](Demo.mp4)

---

# **Thank you**

### **@danielctull**