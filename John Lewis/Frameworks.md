slidenumbers: false

# Frameworks
## A guide to modularising our future

---

# Goals

* Reduce complexity
* Don’t increase implementation time, reduce if possible
* Simplify JLAPI
* Clearer responsibilities
* Reduce code
* Reduce test time
* Reusability of approach, if not code
* Make development fun

---

# Reduce Test Time

* Isolating features
* Parallel builds
* Potential to run tests in parallel

---

# Reduce Complexity

* Clearer responsibilities
* Experimentation allows for simpler ideas
* Isolation of responsibilities allows focus and reduces noise
* Easier to guarantee test coverage


---

# Comparison with VIPER





