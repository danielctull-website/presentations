slidenumbers: false


# Sequences

---

# What is a Sequence?

> A type that provides sequential, iterated access to its elements.
-- Swift Documentation

---

# What is a Sequence?

```swift
for number in [0,1,2,3,4,5] {

    print(number)
}
```
---

# What is a Sequence?

```swift
for number in [0,1,2,3,4,5] {
//            ↳ Sequence ↵
    print(number)
}
```

---

# What is a Sequence?

```swift
public protocol Sequence {

  associatedtype Element

  /// A type that provides the sequence's iteration interface and
  /// encapsulates its iteration state.
  associatedtype Iterator : IteratorProtocol where Iterator.Element == Element

  /// Returns an iterator over the elements of this sequence.
  func makeIterator() -> Iterator
}
```

---

# IteratorProtocol

> A type that supplies the values of a sequence one at a time.
-- Swift Documentation

^ Whenever you use a for-in loop with an array, set, or any other collection or sequence, you’re using that type’s iterator.

---

# IteratorProtocol

```swift
public protocol IteratorProtocol {

  /// The type of element traversed by the iterator.
  associatedtype Element

  /// Advances to the next element and returns it, or `nil` if no next element
  /// exists.
  ///
  /// - Returns: The next element in the underlying sequence, if a next element
  ///   exists; otherwise, `nil`.
  mutating func next() -> Element?
}
```

---

# Why do we care?

Let's create an attributed string with different styles for text separated by commas.

![inline](Sequence-textstyle-comma.png)

---

# Joining an array of strings

There's a sequence extension for joining strings…

```swift
["£38.00", "£33.00", "£27.00"].joined(separator: " , ")
```

…but not for joining NSAttributedStrings

---

# Joining NSAttributedStrings

<!-- ---

```swift
extension Sequence where Element == NSAttributedString {

    public func joined(
        separator: NSAttributedString = NSAttributedString()
    ) -> NSAttributedString {

        let result = NSMutableAttributedString()

        var isFirst = true

		for string in self {

            if !isFirst {
                result.append(separator)
            }

			result.append(string)
            isFirst = false
		}

		return result
    }
}
``` -->

---

```swift
extension Sequence where Element == NSAttributedString {

    public func joined(
        separator: NSAttributedString = NSAttributedString()
    ) -> NSAttributedString {

        let result = NSMutableAttributedString()

		for (index, string) in self.enumerated() {

			if index != 0 {
				result.append(separator)
			}

			result.append(string)
		}

		return result
    }
}
```

---

# What's the problem?

This method does three things:

* Creates and builds the attributed string
* Loops over all elements
* Only appends the separator after the first element

Can we break these out to separate testable pieces?

---

# Using Reduce

```swift
extension Sequence where Element == NSAttributedString {

    public func joined(
        separator: NSAttributedString = NSAttributedString()
    ) -> NSAttributedString {

        return enumerated().reduce(NSMutableAttributedString()) {
			result, item in

			let (index, element) = item

			if index != 0 {
				result.append(separator)
			}

			result.append(element)
			return result
		}
    }
}
```

---

# What's the core of the problem?

We want a sequence that will insert separators into a sequence.

---

# Benefits

Can work for any sequence, so can easily be tested with completely unrelated code:

```swift
["1", "2", "3", "4"].interspersed(with: "-")

// ["1", "-", "2", "-", "3", "-", "4"]



"ABCDE".interspersed(with: "+")

// "A+B+C+D+E"
```

---

# Questions to Ask

* Is this a truly general problem?
* Can this be reused in multiple places?
* Is it hard enough of a problem to get wrong?

---

# The Swift Standard Library solution

Create sequence "wrappers" that solve the issue

Example: calling `reverse` on an array of strings will return a `ReversedCollection<Array<String>>`

When enumerating, the reversed collection starts at the end of the array and works forwards; that's it!

---

# Creating our own wrapper Sequence

---

# Create the Sequence

```swift
public struct InterspersedSequence<Base: Sequence> {

	public typealias Element = Base.Element

	private let base: Base
	private let separator: Element

	public init(base: Base, separator: Element) {
		self.base = base
		self.separator = separator
	}
}
```

---

# Create the Iterator

```swift
extension InterspersedSequence {

	public struct Iterator  {

		private enum State {
			case start
			case base(element: Element)
			case separator
		}

		public typealias Element = Base.Element

		private var base: Base.Iterator
		private let separator: Element
		private var state = State.start

		init(base: Base.Iterator, separator: Element) {
			self.base = base
			self.separator = separator
		}
	}
}
```

---

# Conforming to IteratorProtocol

```swift
extension InterspersedSequence.Iterator: IteratorProtocol {

	public mutating func next() -> Element? {

		switch state {

            case .start:
                state = .separator
                return base.next()

            case .separator:
                guard let next = base.next() else { return nil }

                state = .base(element: next)
                return separator

            case .base(let element):
                state = .separator
                return element
		}
	}
}
```

---

# Conforming to Sequence

```swift
extension InterspersedSequence: Sequence {

	public func makeIterator() -> InterspersedSequence<Base>.Iterator {
		return Iterator(base: base.makeIterator(), separator: separator)
	}
}
```

---

# Adding a convenience method

```swift
extension Sequence {

	public func interspersed(with separator: Element) -> InterspersedSequence<Self> {
		return InterspersedSequence(base: self, separator: separator)
	}
}
```

---

# The solution for joining NSAttributedStrings

```swift
extension Sequence where Element == NSAttributedString {

    public func joined(
        separator: NSAttributedString = NSAttributedString()
    ) -> NSAttributedString {

        return interspersed(with: separator)
            .reduce(NSMutableAttributedString()) {

            result, element in

            result.append(element)
            return result
        }
    }
}
```

---

# Using the joined function

```swift
let historicStyle = TextStyle(characteristics: .title,
                              colors: .primary,
                              additionalAttributes: [.strikethroughStyle(.single)])

let commaStyle = TextStyle(characteristics: .title, colors: .primary)

let comma = NSAttributedString(string: ", ", textStyle: commaStyle)

let historicPrice = historicPrices
            .map { NSAttributedString(string: $0, textStyle: historicStyle) }
            .joined(separator: comma)
```
