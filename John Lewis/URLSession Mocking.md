slidenumbers: false

# URLSession Mocking

---

# What are the limitations of Plexus Mocks?

* Injects code into Plexus when running under test
* Testing system is intwined with Plexus
* Behind the scenes magic to hook up differing responses
    * Not obvious that it happens
    * Surprising results?

---

# URLProtocol

* An API that has existed since the start of time (macOS 10.2)
* Provides a way to add support for new schemes
    * Likely used internally for http, https, ftp, ftps
* Developers can add new custom protocols
* Custom protocols are called before standard ones
* We can use this to override calls made from URLSession

---

# Creating a custom URL Protocol

```swift
public protocol URLProtocolClient: NSObjectProtocol {
    class func canInit(with: URLRequest) -> Bool
    class func canonicalRequest(for: URLRequest) -> URLRequest
    open func startLoading()
    open func stopLoading()
}
```

---

```swift
class func canInit(with: URLRequest) -> Bool
```

* URL protocols are given as classes
* They aren't initialized yet
* Can instances of the URL protocol handle the given URL request?

---

```swift
class func canonicalRequest(for: URLRequest) -> URLRequest
```

>>> It is up to each concrete protocol implementation to define what “canonical” means. A protocol should guarantee that the same input request always yields the same canonical form.

---

```swift
open func startLoading()
```

* Loads the data for the URL request
* Provide feedback to the URL loading system:

```swift
client?.urlProtocol(self, didReceive: response,
					cacheStoragePolicy: .notAllowed)

client?.urlProtocol(self, didLoad: data)

client?.urlProtocol(self, didFailWithError: error)
```

---

```swift
open func stopLoading()
```

* Should stop loading the resource
* But let's just ignore it for now…

---

# Creating a custom URLSession

```swift
public typealias URLResult = Result<(URLResponse, Data)>

extension URLSession {

    // Provide an override function
	public convenience init(override: @escaping (URLRequest) -> (URLResult?)) {

        // Register the OverrideURLProtocol class
        URLProtocol.registerClass(OverrideURLProtocol.self)

        // Set the static variable of OverrideURLProtocol
		OverrideURLProtocol.override = override

        // Add OverrideURLProtocol to the array of protocol classes.
		let configuration = URLSessionConfiguration.default
		configuration.protocolClasses = [OverrideURLProtocol.self]
		self.init(configuration: configuration)
	}
}
```

---

# Success Tests Usage

```swift
let bundle = Bundle(for: BackInStockKitTests.self)
let url = try AssertNotNil(bundle.url(forResource: "BackInStock", withExtension: "json"))
let data = try Data(contentsOf: url)

let session = URLSession(override: { (url: URL) in
    return Result(url: url, data: data)
})

session.requestBackInStockEmailNotification(
    customerID: "customer_123",
    customerEmail: "customer@johnlewis.com") { result in

    // test the result is successful
}
```

---

# Result Conveniences

```swift
extension Result where Value == (URLResponse, Data) {

	public init(url: URL, data: Data) {
		let response = HTTPURLResponse(url: url,
            statusCode: 200,
            httpVersion: nil,
            headerFields: nil)!
		self = .success((response, data))
	}

    public init(url: URL, statusCode: Int, error: Error) {
        // let response = HTTPURLResponse(url: url,
        //     statusCode: statusCode,
        //     httpVersion: nil,
        //     headerFields: nil)
        self = .failure(error)
    }
}
```

---

# Failing Tests Usage

```swift
let expectedError = APIError(statusCode: .unknown)
let session = URLSession(override: { (url: URL) in
    return Result(url: url, statusCode: 404, error: expectedError)
})

session.requestBackInStockEmailNotification(
    customerID: "customer_123",
    customerEmail: "customer@johnlewis.com") { result in

    // test the result
}
```

---

# Usage Support

|            | URL Protocol                                                      | Plexus Mocks                            |
|------------|-------------------------------------------------------------------|-----------------------------------------|
| Unit Tests | Easy to test different responses                                  | Possible to test different responses    |
| UI Tests   | Not really possible – needs work, could allow different responses | Possible, but only one set of responses |
| Demo Apps  | Easy to inject URL session & provide different responses          | X                                       |

---

# Others Comparisons

* Zero code needed in app to implement mocking
* Independent of Plexus
    * Can replace Plexus without breaking tests
    * Can replace tests without changing Plexus
* Easier to stub multiple responses
* Closer to the "metal"; hits all the real URLSession code
* Doesn't work for UI tests
